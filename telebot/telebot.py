import sys
import time
import telepot
from telepot.namedtuple import ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardHide, ForceReply
import json
import requests
import uuid
import re
from enum import Enum
from pprint import pprint

BOT_REPLY_SERVICE_URL = "http://localhost:8087/bot/reply"

HELP_MESSAGE = "Hi, you can configure me to communicate in some person style.\n" \
               "\n" \
               "/configure - choose the person in whose style would you like me to communicate.\n" \
               "\n" \
               "When you choose, you can communicate with me as a person.\n"

NOT_CONFIGURED_MESSAGE = "Hi, I'm not configured to communicate in some person style.\n" \
                         "\n" \
                         "/configure - choose the person in whose style would you like me to communicate.\n" \
                         "\n" \
                         "When you choose, you can communicate with me as a person.\n"

PERSONS_PROPOSAL_MESSAGE = "Please choose a person in keyboard below or print exact name. {}.\n"

WRONG_CONFIGURED_MESSAGE = "Please choose correct person.\n"

CONFIGURED_SUCCESSFUL_MESSAGE = "Hi, I'm successfully configured. Now I'm {}.\n"

OUTDATED_MESSAGE = "Hi, our conversation was outdated. Please /configure me and can start again.\n"


class ConversationStatus(Enum):
    NONE = 1
    CONFIGURING = 2
    CONFIGURED = 3


class Conversation():
    CONVERSATION_TIMEOUT_SEC = 3600

    def __init__(self, date):
        self.id = uuid.uuid4()
        self.status = ConversationStatus.NONE
        self.date = date
        self.person = ''

    def is_outdated(self, msg_date):
        return self.status == ConversationStatus.CONFIGURED \
               and msg_date - self.date > Conversation.CONVERSATION_TIMEOUT_SEC


class MultiPersonBot(telepot.Bot):

    def __init__(self, *args, **kwargs):
        super(MultiPersonBot, self).__init__(*args, **kwargs)
        self._bot_id = self.getMe()['username']
        self._answerer = telepot.helper.Answerer(self)
        self._web_client = BotWebClient(BOT_REPLY_SERVICE_URL)
        self._conversations = dict()
        # TODO Get available persons from service
        self._persons = {'Pushkin', 'Trump', 'Your Own Model'}

    def _send_personalized_message(self, chat_type, chat_id, msg, text, reply_markup=None):
        if chat_type == 'group':
            self.sendMessage(chat_id, text, reply_to_message_id=msg['message_id'], reply_markup=reply_markup)
        else:
            self.sendMessage(chat_id, text, reply_markup=reply_markup)

    @staticmethod
    def _get_conversation_id(chat_type, msg):
        if chat_type == 'group':
            return msg['from']['id'], msg['chat']['id']
        else:
            return msg['from']['id'], 0

    def _get_current_conversation(self, chat_type, msg):
        # Get or initialize conversation
        conversation_id = self._get_conversation_id(chat_type, msg)

        if conversation_id in self._conversations.keys():
            conversation = self._conversations[conversation_id]
        else:
            conversation = Conversation(msg['date'])
            self._conversations[conversation_id] = conversation

        return conversation

    def _handle_configured_person(self, chat_type, chat_id, conversation, msg, person):
        if person in self._persons:
            conversation.status = ConversationStatus.CONFIGURED
            conversation.person = person
            self._send_personalized_message(chat_type, chat_id, msg, CONFIGURED_SUCCESSFUL_MESSAGE.format(person))
        else:
            self._send_personalized_message(chat_type, chat_id, msg, WRONG_CONFIGURED_MESSAGE)

    def _is_message_for_bot(self, chat_type, msg):
        if chat_type == 'group' and str(msg['text']).find("@" + self._bot_id) == -1:
            return None
        return 1

    @staticmethod
    def _remove_mentions(msg):
        return re.sub("@[^ ]+", "", str(msg['text'])).strip()


    def on_chat_message(self, msg):
        content_type, chat_type, chat_id = telepot.glance(msg)

        if not self._is_message_for_bot(chat_type, msg):
            return

        conversation = self._get_current_conversation(chat_type, msg)

        # Check if conversation is outdated
        if conversation.is_outdated(msg['date']):
            conversation.status = ConversationStatus.NONE
            self._send_personalized_message(chat_type, chat_id, msg, OUTDATED_MESSAGE)
            return

        # Get message text and remove mentions
        message = self._remove_mentions(msg)

        # Handle help message
        if message == '/help':
            self._send_personalized_message(chat_type, chat_id, msg, HELP_MESSAGE)
        # Handle configure message
        elif message == '/configure':
            conversation.status = ConversationStatus.CONFIGURING
            # TODO Make proposal with keyboard
            self._send_personalized_message(chat_type, chat_id, msg, PERSONS_PROPOSAL_MESSAGE.format(self._persons))
        else:
            if conversation.status == ConversationStatus.NONE:
                self._send_personalized_message(chat_type, chat_id, msg, NOT_CONFIGURED_MESSAGE)
            elif conversation.status == ConversationStatus.CONFIGURING:
                self._handle_configured_person(chat_type, chat_id, conversation, msg, message)
            else:
                # Delete mentions (/something) and commands (@something) from message
                cleaned_message = re.sub(r"/[^ ]+", "", message.lower()).strip()
                # Get answer from web application
                answer = self._web_client.forward_message(conversation.id, conversation.person, cleaned_message)
                self._send_personalized_message(chat_type, chat_id, msg, answer)

        pprint(msg)  # For debug

    def on_callback_query(self, msg):
        query_id, from_id, query_data = telepot.glance(msg, flavor='callback_query')
        print('Callback Query:', query_id, from_id, query_data)

    def on_inline_query(self, msg):
        query_id, from_id, query_string = telepot.glance(msg, flavor='inline_query')
        print('Inline Query:', query_id, from_id, query_string)

        def compute_answer():
            # Compose your own answers
            articles = [{'type': 'article',
                         'id': 'abc', 'title': query_string, 'message_text': query_string}]

            return articles

        self._answerer.answer(msg, compute_answer)

    def on_chosen_inline_result(self, msg):
        result_id, from_id, query_string = telepot.glance(msg, flavor='chosen_inline_result')
        print('Chosen Inline Result:', result_id, from_id, query_string)


class BotWebClient(object):
    def __init__(self, url):
        self._url = url

    def forward_message(self, conversation_id, person, msg):
        try:
            response = requests.get(self._url,
                                    params=dict(conversation_id=conversation_id, person=person, msg=msg))
            status = response.status_code
            if status == 200:
                json_answer = json.loads(response.text)
                return json_answer['answer']
            else:
                return 'Something wrong with bot_web_app server. Response status is ' + status
        except Exception as e:
            print('ERROR ', e)
            return 'ERROR!'


if __name__ == '__main__':
    TOKEN = sys.argv[1]  # get token from command-line

    bot = MultiPersonBot(TOKEN)
    bot.message_loop()
    print('Listening ...')

    # Keep the program running.
    while 1:
        time.sleep(10)
