class BotReplyService(object):
    REPLY_TEMPLATE = "Your messages count is {}. Person is {}."

    def __init__(self):
        # Simple counter of messages
        self._conversations = dict()

    def reply(self, conversation_id, person, msg):

        if conversation_id in self._conversations.keys():
            self._conversations[conversation_id] += 1
        else:
            self._conversations[conversation_id] = 1

        return BotReplyService.REPLY_TEMPLATE.format(self._conversations[conversation_id], person)
        # TODO
