from bot_service.service import BotReplyService
from flask import Flask, jsonify, request

app = Flask(__name__)
bot_service = BotReplyService()


@app.route('/bot/reply', methods=['GET'])
def get_reply():
    conversation_id = request.args.get('conversation_id')
    person = request.args.get('person')
    msg = request.args.get('msg')
    answer = bot_service.reply(conversation_id, person, msg)
    return jsonify({'answer': answer})


if __name__ == '__main__':
    app.run(port=8087, debug=True)
